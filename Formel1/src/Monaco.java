
public class Monaco
{

	public static void main(String[] args)
	{
		Auto auto1 = new Auto("rot"); // referenz zeigt wo die Sache ist so auto1 is reference variable

		Auto auto2 = new Auto("blau");
		
		auto1.status();
		auto2.status(); 
	//	System.out.println(auto1.toString()); //gleich wie System.out.println(auto1)
		
		auto1.volltanken();
		auto2.volltanken();

		auto1.status();
		auto1.beschleunigen(10);
		auto2.status();
		
		auto1.fahren(120);
		auto1.status();
	}
	

}
