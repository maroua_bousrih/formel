
public class Auto
{
	
	private String farbe;
	private double tankstand;
	private double geschwindigkeit;
	
	//constructor hei�t wie die Klasse 
	public Auto(String autofarbe)
	{
		farbe=autofarbe;
	}
	
	public void volltanken()
	{
		tankstand = 100;
	}
	
	public void beschleunigen(double gas)
	{
		
		geschwindigkeit = geschwindigkeit  + gas;
		if (geschwindigkeit >350) 
		{
			geschwindigkeit = 350;
		}
		
	}
	
	public void langsamer(double bremsen) 
	{
		geschwindigkeit = geschwindigkeit - bremsen;
		if (geschwindigkeit <=0)
		{
			geschwindigkeit=0;
		}
	}
	public void fahren(double zeit) //sekunden
	{
		tankstand = tankstand - (geschwindigkeit * zeit) / 7000;
		checkStatus();
		
	}

	public void status()
	{
		System.out.println(farbe + " " + geschwindigkeit+ " " + tankstand );
	}
	// Einsatz von status() methode
//	public String toString()
//	{
//		return farbe+ " " + geschwindigkeit;
//				
//	}
	
	private void checkStatus()
	{
		if (tankstand <=0)
		{
			tankstand = 0;
			geschwindigkeit = 0;
		}
	}

}
